package com.epam.training.toto.domain;

import java.time.LocalDate;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

public class Hit {
  private String date;
  private List<String> hits;
  private HashMap<Integer, String> prizes = new HashMap<>();

  public Hit(List<String> round) {
    hits = round
        .stream()
        .skip(14)
        .map(el -> el.contains("+") ? el.replace("+", "") : el)
        .collect(Collectors.toList());

    date = round.get(Constants.DATE_COLUMN_INDEX)
        .substring(0, round.get(Constants.DATE_COLUMN_INDEX).length() - 1)
        .replace(".", "-");

    prizes.put(14, round.get(Constants.FIRST_PRIZE_COLUMN_INDEX));
    prizes.put(13, round.get(Constants.SECOND_PRIZE_COLUMN_INDEX));
    prizes.put(12, round.get(Constants.THIRD_PRIZE_COLUMN_INDEX));
    prizes.put(11, round.get(Constants.FOURTH_PRIZE_COLUMN_INDEX));
    prizes.put(10, round.get(Constants.FOURTH_PRIZE_COLUMN_INDEX));
  }

  public String checkHits(List<String> inputHits) {
    Integer numberOfHits = calculateNumberOfHits(hits, inputHits);

    if (numberOfHits < 10) {
      return "Unfortunately, you didn't win anything";
    }

    String prize = prizes.get(numberOfHits);
    return "Result: hits: " + numberOfHits + ", amount: " + prize;
  }

  public LocalDate getDate() {
    return LocalDate.parse(date);
  }

  private Integer calculateNumberOfHits(List<String> arr1, List<String> arr2) {
    Integer count = 0;
    for (int i=0; i < arr1.size(); i++) {
      if (arr1.get(i).equals(arr2.get(i))) {
        count++;
      }
    }
    return count;
  }
}

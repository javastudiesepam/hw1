package com.epam.training.toto.domain;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


public class Round {
  private String maxPrize;
  private List<String> distributionArray;

  public Round(List<String> roundsList) {
    maxPrize = roundsList.get(5);

    distributionArray = roundsList
        .stream()
        .skip(14)
        .map(el -> el.contains("+") ? el.replace("+", "") : el)
        .collect(Collectors.toList());
  }

  public Map<Outcome, String> getDistribution() {
    Map<Outcome, String> distribution = new HashMap<>();

    distribution.put(Outcome.Team1, Utils.formatPercentage(getDistribution(distributionArray, "1")));
    distribution.put(Outcome.Team2, Utils.formatPercentage(getDistribution(distributionArray, "2")));
    distribution.put(Outcome.Draw, Utils.formatPercentage(getDistribution(distributionArray, "X")));

    return distribution;
  }

  public Integer getMaxPrize() {
    return currencyToInt(maxPrize);
  }

  private double getDistribution(List<String> distributionList, String val) {
    double results = distributionList
        .stream()
        .filter(el -> el.contains(val))
        .count();
    return results / distributionList.size();
  }

  private Integer currencyToInt(String curr) {
    String strippedStr = curr.replaceAll(Constants.NOT_DIGITS_REGEXP, "");
    return Integer.parseInt(strippedStr);
  }
}

package com.epam.training.toto.domain;

public enum  Outcome {
  Team1, Team2, Draw
}

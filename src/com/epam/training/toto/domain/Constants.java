package com.epam.training.toto.domain;

public class Constants {
  public static final String CSV_SEPARATOR = ";";
  public static final String UA_FORMAT_PATTERN = "###,### UAH";
  public static final String PERCENTAGE_FORMAT_PATTERN = "###.## %";
  public static final Integer DATE_COLUMN_INDEX = 3;
  public static final Integer FIRST_PRIZE_COLUMN_INDEX = 5;
  public static final Integer SECOND_PRIZE_COLUMN_INDEX = 7;
  public static final Integer THIRD_PRIZE_COLUMN_INDEX = 9;
  public static final Integer FOURTH_PRIZE_COLUMN_INDEX = 11;
  public static final Integer FIFTH_PRIZE_COLUMN_INDEX = 13;
  public static final String NOT_DIGITS_REGEXP = "[^\\d.,]";
}

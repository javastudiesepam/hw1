package com.epam.training.toto.domain;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Utils {
  public static String formatCurrency(Integer amount) {
    Locale uaLocale = new Locale("ua", "UA");

    DecimalFormatSymbols uaSymbols = new DecimalFormatSymbols(uaLocale);
    uaSymbols.setDecimalSeparator(',');
    uaSymbols.setGroupingSeparator(' ');

    DecimalFormat uaFormatter = new DecimalFormat(Constants.UA_FORMAT_PATTERN, uaSymbols);

    return uaFormatter.format(amount);
  }

  public static <T> List<T> readCsvFile(Function<String, T> initRound, String filePath) {
    List<T> roundsList = new ArrayList<>();

    try (Stream<String> stream = Files.lines(Paths.get(filePath))) {

      roundsList = stream
          .filter(line ->
              line.split(Constants.CSV_SEPARATOR)[Constants.DATE_COLUMN_INDEX].length() != 0 //exclude line without date
          )
          .map(initRound)
          .collect(Collectors.toList());

    } catch (IOException e) {
      e.printStackTrace();
    }
    return roundsList;
  }

  public static String formatPercentage(double amount) {
    return new DecimalFormat(Constants.PERCENTAGE_FORMAT_PATTERN, new DecimalFormatSymbols()).format(amount);
  }
}

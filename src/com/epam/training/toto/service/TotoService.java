package com.epam.training.toto.service;

import com.epam.training.toto.domain.*;

import java.time.DateTimeException;
import java.time.LocalDate;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;


public class TotoService {
  private static final String filePath = "src/toto.csv";

  public String getMaxPrize() {
    List<Round> roundsList = Utils.readCsvFile(initRound, filePath);

    Integer maxPrize = roundsList
        .stream()
        .map(Round::getMaxPrize)
        .max(Integer::compare)
        .orElse(0);

    return Utils.formatCurrency(maxPrize);
  }

  public List<Map<Outcome, String>> getDistributionList() {
    List<Round> roundsList = Utils.readCsvFile(initRound, filePath);

    return roundsList
        .stream()
        .map(Round::getDistribution)
        .collect(Collectors.toList());
  }

  public String getHits(String inputDate, String inputHits) {

    String formattedInputDate = inputDate
        .substring(0, inputDate.length() - 1)
        .replace(".", "-");

    LocalDate localInputDate;
    try {
      localInputDate = LocalDate.parse(formattedInputDate);
    } catch (DateTimeException e) {
      return "Unfortunately, the date is incorrect";
    }

    List<Hit> hitsList = Utils.readCsvFile(initHit, filePath);

    Hit targetHit = hitsList
        .stream()
        .filter(hit -> hit.getDate().equals(localInputDate))
        .findFirst()
        .orElse(null);

    if (targetHit == null) {
      return "Unfortunately, there were no rounds at this date";
    }

    return targetHit.checkHits(Arrays.asList(inputHits.split("")));
  }

  private Function<String, Round> initRound = line -> new Round(Arrays.asList(line.split(Constants.CSV_SEPARATOR)));
  private Function<String, Hit> initHit = line -> new Hit(Arrays.asList(line.split(Constants.CSV_SEPARATOR)));
}

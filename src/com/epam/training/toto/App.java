package com.epam.training.toto;

import com.epam.training.toto.domain.Outcome;
import com.epam.training.toto.service.TotoService;
import java.util.List;
import java.util.Map;

/*
* TODO:
* DONE - fix relative path
* DONE - [] --> List
* DONE - int --> Integer
* DONE - float --> double
* DONE - Methods order: public, protected, private
* DONE - create constants class with static methods and utils class
* */


public class App {

  public static void main(String[] args) {
    TotoService totoService = new TotoService();

    System.out.println("the maximum prize was " + totoService.getMaxPrize());


    List<Map<Outcome, String>> distributionList = totoService.getDistributionList();

    for (Map<Outcome, String> roundDistribution: distributionList) {
      System.out.println(
          "team #1 won: " + roundDistribution.get(Outcome.Team1) + ", " +
          "team #2 won: " + roundDistribution.get(Outcome.Team2) + ", " +
          "draw: " + roundDistribution.get(Outcome.Draw)
      );
    }


    String inputDate = "2015.10.29.";
    String inputHits = "21112221211112";

    String hitsInfo = totoService.getHits(inputDate, inputHits);
    System.out.println(hitsInfo);
  }
}
